package com.ksa.accountbookapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum CategoryStatus {
    INCOME("수입"),
    SPENDING("지출");

    private final String name;
}
