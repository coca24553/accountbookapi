package com.ksa.accountbookapi.entity;

import com.ksa.accountbookapi.enums.CategoryStatus;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private LocalDate dateCreate;

    @Column(nullable = false, length = 10)
    @Enumerated(value = EnumType.STRING)
    private CategoryStatus categoryStatus;

    @Column(nullable = false)
    private Long money;

    @Column(nullable = false, length = 30)
    private String moneyCategory;

    @Column(columnDefinition = "TEXT")
    private String etcMemo;
}
