package com.ksa.accountbookapi.repository;

import com.ksa.accountbookapi.entity.Category;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryRepository extends JpaRepository<Category, Long> {
}
