package com.ksa.accountbookapi.controller;

import com.ksa.accountbookapi.model.CategoryItem;
import com.ksa.accountbookapi.model.CategoryRequest;
import com.ksa.accountbookapi.service.CategoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/category")
public class CategoryController {
    private final CategoryService categoryService;

    @PostMapping("/new")
    public String setCategory(@RequestBody CategoryRequest request) {
        categoryService.setCategory(request);

        return "OK";
    }

    @GetMapping("/type")
    public List<CategoryItem> getCategory() {
        return categoryService.getCategory();
    }
}
