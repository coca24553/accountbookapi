package com.ksa.accountbookapi.service;

import com.ksa.accountbookapi.entity.Category;
import com.ksa.accountbookapi.model.CategoryItem;
import com.ksa.accountbookapi.model.CategoryRequest;
import com.ksa.accountbookapi.repository.CategoryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CategoryService {
    private final CategoryRepository categoryRepository;

    public void setCategory(CategoryRequest request) {
        Category addDate = new Category();
        addDate.setDateCreate(LocalDate.now());
        addDate.setCategoryStatus(request.getCategoryStatus());
        addDate.setMoney(request.getMoney());
        addDate.setMoneyCategory(request.getMoneyCategory());
        addDate.setEtcMemo(request.getEtcMemo());

        categoryRepository.save(addDate);
    }

    public List<CategoryItem> getCategory() {
        List<Category> originList = categoryRepository.findAll();

        List<CategoryItem> result = new LinkedList<>();

        for (Category category : originList) {
            CategoryItem addItem = new CategoryItem();
            addItem.setId(category.getId());
            addItem.setDateCreate(category.getDateCreate());
            addItem.setCategoryStatus(category.getCategoryStatus().getName());
            addItem.setMoney(category.getMoney());
            addItem.setMoneyCategory(category.getMoneyCategory());

            result.add(addItem);
        }

        return result;
    }
}
