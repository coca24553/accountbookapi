package com.ksa.accountbookapi.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class CategoryItem {
    private Long id;
    private LocalDate dateCreate;
    private String categoryStatus;
    private Long money;
    private String moneyCategory;
}
