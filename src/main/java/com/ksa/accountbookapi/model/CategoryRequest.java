package com.ksa.accountbookapi.model;

import com.ksa.accountbookapi.enums.CategoryStatus;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class CategoryRequest {
    @Enumerated(value = EnumType.STRING)
    private CategoryStatus categoryStatus;
    private Long money;
    private String moneyCategory;
    private String etcMemo;
}
